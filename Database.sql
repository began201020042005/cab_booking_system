SQL> create table passenger(
  2  passenger_id number primary key,
  3  passenger_name varchar2(26),
  4  passenger_phonenumber varchar2(10) unique,
  5  passenger_email varchar2(50) unique,
  6  passenger_address varchar2(50),
  7  passenger_dob date,
  8  passenger_username varchar2(16) unique.
  9  passenger_password varchar2(16)
  10  );

Table created.

SQL> desc passenger;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 PASSENGER_ID                                      NOT NULL NUMBER
 PASSENGER_NAME                                             VARCHAR2(26)
 PASSENGER_PHONENUMBER                                      VARCHAR2(10)
 PASSENGER_EMAIL                                            VARCHAR2(50)
 PASSENGER_ADDRESS                                          VARCHAR2(50)
 PASSENGER_DOB                                              DATE
 PASSENGER_USERNAME                                         VARCHAR2(16)
 PASSENGER_PASSWORD                                         VARCHAR2(16)

SQL> create table passengerlogin(
  2  passengerlogin_name varchar2(26),
  3  passengerlogin_username varchar2(20) unique,
  4  passengerlogin_password varchar2(20)
  5  );

Table created.

SQL> desc passengerlogin;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 PASSENGERLOGIN_NAME                                             VARCHAR2(26)
 PASSENGERLOGIN_USERNAME                                         VARCHAR2(20)
 PASSENGERLOGIN_PASSWORD                                         VARCHAR2(20)

SQL> create table admin(
  2  admin_name varchar2(26),
  3  admin_username varchar2(20) primary key,
  4  admin_password varchar2(20)
  5  );

SQL> desc admin;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 ADMIN_NAME                                             VARCHAR2(26)
 ADMIN_USERNAME                            not null     VARCHAR2(20)
 ADMIN_PASSWORD                                         VARCHAR2(20)

SQL> insert all
  2  into admin values('Tom','MJTB0232406','P@ssw0rd')
  3  into admin values('Jerry','MJTB0232408','P@ssw0rd')
  4  into admin values('Peter','MJTB0232408','P@ssw0rd')
  5  select * from dual;

3 rows created.

SQL> commit;

Commit complete.

SQL> create table driver(
  2  driver_id number primary key,
  3  driver_name varchar2(26),
  4  driver_email varchar2(50) unique,
  5  driver_phonenumber varchar2(10),
  6  driver_experience number,
  7  driver_License varchar2(30)
  8  );

Table created.

SQL> desc driver
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 D_ID                                      NOT NULL NUMBER
 D_NAME                                             VARCHAR2(26)
 D_EMAIL                                            VARCHAR2(50)
 D_PHONENUMBER                                      VARCHAR2(10)
 D_EXPERIENCE                                       NUMBER
 D_LICENSE                                          VARCHAR2(30)

SQL> create table driverlogin(
  2  driverlogin_name varchar2(26),
  3  driverlogin_username varchar2(20),
  4  driverlogin_password varchar2(20)
  5  );

Table created.

SQL> desc driverlogin;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 DRIVERLOGIN_NAME                                             VARCHAR2(26)
 DRIVERLOGIN_USERNAME                                         VARCHAR2(20)
 DRIVERLOGIN_PASSWORD                                         VARCHAR2(20)
 
SQL> create table cardetails(
  2  cardetails_id int primary key,
  3  cardetails_number varchar2(12) unique,
  4  cardetails_rating number(1),
  5  cardetails_payamount number(10),
  6  check (c_rating<=5)
  7  );

Table created.

SQL> desc cardetails;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 CARDETAILS_ID                                      NOT NULL NUMBER(38)
 CARDETAILS_NUMBER                                           VARCHAR2(12)
 CARDETAILS_RATING                                           NUMBER(1)
 CARDETAILS_PAYAMOUNT                                        NUMBER(10)

 SQL> create table booking(
 		booking_id number primary key, 
 		car_id number references cardetails(c_id),
 		booking_date date,passenger_mobile varchar2(10)
 		);
 		
SQL> desc booking;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 BOOKING_ID                                NOT NULL NUMBER
 CAR_ID                                             NUMBER
 BOOKING_DATE                                       DATE
 PASSENGER_MOBILE                                   VARCHAR2(10)
